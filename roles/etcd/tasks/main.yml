---
# Корневой сертифика CA etcd
- name: Creates directory
  delegate_to: localhost
  file: 
    path: "{{ ssl_etcd_dir }}"
    state: directory

- name: Generate config for node certificate creation
  delegate_to: localhost
  template:
    src: 'ca.conf.j2'
    dest: "{{ ssl_etcd_dir }}/ca.conf"

- name: Stat root CA private key file
  delegate_to: localhost
  stat:
    path: "{{ ssl_etcd_dir }}/rootCA.key"
  register: root_ca_key_stat

- name: Stat root CA certificate file
  delegate_to: localhost
  stat:
    path: "{{ ssl_etcd_dir }}/rootCA.crt"
  register: root_ca_crt_stat

- name: Generate/regenerate root CA private key
  delegate_to: localhost
  shell: |
    openssl req \
    -batch \
    -nodes \
    -new -x509 \
    -config {{ ssl_etcd_dir }}/ca.conf \
    -extensions v3_ca \
    -days {{ ssl_etcd_days_valid }} \
    -keyout {{ ssl_etcd_dir }}/rootCA.key \
    -out {{ ssl_etcd_dir }}/rootCA.crt   
  when:
    ssl_etcd_regenerate|bool == true or root_ca_key_stat.stat.exists == false or root_ca_crt_stat.stat.exists == false 
    
# Генерация ключей для etcd
- name: Create each host key directory
  delegate_to: localhost
  file: 
    path: "{{ ssl_etcd_dir }}/{{ ansible_hostname }}"
    state: directory
  when: 
    ansible_hostname in groups['etcd-master']    

- name: Generate config for node certificate creation
  delegate_to: localhost
  template:
    src: 'node.conf.j2'
    dest: "{{ ssl_etcd_dir }}/{{ ansible_hostname }}/node.conf"
  when: 
    ansible_hostname in groups['etcd-master']   

- name: Stat etcd private key file
  delegate_to: localhost
  stat:
    path: "{{ ssl_etcd_dir }}/{{ ansible_hostname }}/etcd.key"
  register: node_key_stat  
  when: 
    ansible_hostname in groups['etcd-master']   

- name: Stat etcd csr files
  delegate_to: localhost
  stat:
    path: "{{ ssl_etcd_dir }}/{{ ansible_hostname }}/etcd.csr"
  register: node_csr_stat  
  when: 
    ansible_hostname in groups['etcd-master']   

- name: Create/recreate private key and csr for each etcd node
  delegate_to: localhost
  shell: |
    openssl req \
    -config {{ ssl_etcd_dir }}/{{ ansible_hostname }}/node.conf \
    -new -nodes -batch \
    -keyout {{ ssl_etcd_dir }}/{{ ansible_hostname }}/etcd.key \
    -out {{ ssl_etcd_dir }}/{{ ansible_hostname }}/etcd.csr
  when:
    ansible_hostname in groups['etcd-master'] and 
    (ssl_etcd_regenerate|bool == true or node_key_stat.stat.exists == false or node_csr_stat.stat.exists == false)

- name: Stat node certificate files
  delegate_to: localhost
  stat:
    path: "{{ ssl_etcd_dir }}/{{ ansible_hostname }}/etcd.crt"
  register: node_cert_stat  
  when: 
    ansible_hostname in groups['etcd-master']   

- name: Create/recreate certificate files for each node
  delegate_to: localhost
  shell: |
    openssl x509 -req \
    -extfile {{ ssl_etcd_dir }}/{{ ansible_hostname }}/node.conf \
    -extensions server \
    -CA {{ ssl_etcd_dir }}/rootCA.crt \
    -CAkey {{ ssl_etcd_dir }}/rootCA.key \
    -CAcreateserial \
    -days {{ ssl_etcd_days_valid }} \
    -in {{ ssl_etcd_dir }}/{{ ansible_hostname }}/etcd.csr \
    -out {{ ssl_etcd_dir }}/{{ ansible_hostname }}/etcd.crt
  when:
    ansible_hostname in groups['etcd-master'] and 
    (ssl_etcd_regenerate|bool == true or node_cert_stat.stat.exists == false)

# Создание папок на ноде
- name: Create etcd docker container directory
  file:
    path: "{{ etcd_dir }}"
    state: directory
  when: 
    ansible_hostname in groups['etcd-master'] 

- name: Create etcd ssl certificate directory
  file:
    path: /etc/ssl/certs/etcd
    state: directory
  when: 
    ansible_hostname in groups['etcd-master'] 

- name: Create etcd ssl key directory
  file:
    path: /etc/ssl/private/etcd
    mode: 0700
    state: directory
  when: 
    ansible_hostname in groups['etcd-master'] 

# Копирование сертификатов
- name: Copy root ca file
  copy:
    src: "{{ ssl_etcd_dir }}/rootCA.crt"
    dest: "/etc/ssl/certs/etcd/rootCA.crt"
  when: 
    ansible_hostname in groups['etcd-master'] 
  notify:
    - etcd restart

- name: Copy node key file
  copy:
    src: "{{ ssl_etcd_dir }}/{{ inventory_hostname }}/etcd.key"
    dest: "/etc/ssl/private/etcd/etcd.key"
    mode: 0600
  when: 
    ansible_hostname in groups['etcd-master'] 
  notify:
    - etcd restart

- name: Copy node cert file
  copy:
    src: "{{ ssl_etcd_dir }}/{{ inventory_hostname }}/etcd.crt"
    dest: "/etc/ssl/certs/etcd/etcd.crt"
  when: 
    ansible_hostname in groups['etcd-master']
  notify:
    - etcd restart

# Сервис etcd  
- name: Generate etcd master config
  with_items:
    - etcd_master.env
  template:
    src: "{{ item }}.j2"
    dest: "{{ etcd_dir }}/{{ item }}"
  when:
    - ansible_hostname in groups['etcd-master']
  notify:
    - etcd restart

- name: Generate etcd proxy config
  with_items:
    - etcd_proxy.env
  template:
    src: "{{ item }}.j2"
    dest: "{{ etcd_dir }}/{{ item }}"
  when:
    - ansible_hostname not in groups['etcd-master']    
  notify:
    - etcd restart

- name: Generate etcd node service and config
  with_items:
    - docker-compose.yml
    - etcd_node.env
  template:
    src: "{{ item }}.j2"
    dest: "{{ etcd_dir }}/{{ item }}"
  when:
    - ansible_hostname in groups['etcd-master']    
  notify:
    - etcd restart

- name: Generate systemd service
  template:
    src: "etcd.service.j2"
    dest: "/etc/systemd/system/etcd.service"
  notify:
    - etcd enable
    - etcd start

- name: Enable and start etcd
  systemd:
    name: etcd
    state: started
    daemon_reload: yes
    enabled: true

- name: Flush handlers
  meta: flush_handlers    