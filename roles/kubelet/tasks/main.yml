---
# Генерация ключей для kubelet    
- name: Stat kubelet key file
  delegate_to: localhost
  stat:
    path: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.key"
  register: kubelet_key_stat

- name: Stat kubelet csr file
  delegate_to: localhost
  stat:
    path: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.csr"
  register: kubelet_csr_stat  

- name: Create/recreate private key and csr for kubelet
  delegate_to: localhost
  shell: |
    openssl req \
    -config {{ ssl_api_server_dir }}/{{ ansible_hostname }}/node.conf \
    -new -nodes -batch \
    -keyout {{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.key \
    -out {{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.csr
  when:
    ssl_api_server_regenerate|bool == true or kubelet_key_stat.stat.exists == false or kubelet_csr_stat.stat.exists == false

- name: Stat kubelet certificate file
  delegate_to: localhost
  stat:
    path: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.crt"
  register: kubelet_cert_stat  

- name: Sign kubelet certificate with root CA
  delegate_to: localhost
  shell: |
    openssl x509 -req \
    -extfile {{ ssl_api_server_dir }}/{{ ansible_hostname }}/node.conf \
    -extensions client \
    -CA {{ ssl_api_server_dir }}/rootCA.crt \
    -CAkey {{ ssl_api_server_dir }}/rootCA.key \
    -CAcreateserial \
    -days {{ ssl_api_server_days_valid }} \
    -in {{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.csr \
    -out {{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.crt
  when:
    ssl_api_server_regenerate|bool == true or kubelet_cert_stat.stat.exists == false

# Копирование сертификатов
- name: Copy kubelet key file
  copy:
    src: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.key"
    dest: "/etc/ssl/private/api_server/kubelet.key"
    mode: 0600
  notify:
    - kubelet restart

- name: Copy kubelet cert file
  copy:
    src: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kubelet.crt"
    dest: "/etc/ssl/certs/api_server/kubelet.crt"
  notify:
    - kubelet restart

# Установка kubelet на ноде    
- name: Install kubelet
  apt:
    name: kubelet
    update_cache: yes

- name: Creates kubelet directory
  file: 
    path: /var/lib/kubelet
    state: directory

- name: Generate extra arguments to kubelet daemon
  template:
    src: config.j2
    dest: "/var/lib/kubelet/config"          
  notify:
    - kubelet restart

- name: Generate kubelet config
  template:
    src: kubelet.j2
    dest: "/var/lib/kubelet/kubeconfig"
  notify:
    - kubelet restart

- name: Create kubelet config directory
  file:
    path: "/etc/systemd/system/kubelet.service.d"
    state: directory

- name: Generate kubelet service
  template:
    src: 10-kubelet.conf.j2
    dest: "/etc/systemd/system/kubelet.service.d/10-kubelet.conf"
  notify:
    - kubelet restart
    
- name: Start and enable autostart of kubelet
  systemd:
    enabled: true
    state: started
    daemon_reload: yes
    name: kubelet

- name: Flush handlers
  meta: flush_handlers     