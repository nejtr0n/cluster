---
# Генерация ключей для kube controller manager    
- name: Stat kube controller manager key file
  delegate_to: localhost
  stat:
    path: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.key"
  register: kube_controller_manager_key_stat
  when: 
    ansible_hostname in groups['cluster-master']

- name: Stat kube controller manager csr file
  delegate_to: localhost
  stat:
    path: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.csr"
  register: kube_controller_manager_csr_stat  
  when: 
    ansible_hostname in groups['cluster-master']

- name: Create/recreate private key and csr for kube controller manager
  delegate_to: localhost
  shell: |
    openssl req \
    -config {{ ssl_api_server_dir }}/{{ ansible_hostname }}/node.conf \
    -new -nodes -batch \
    -keyout {{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.key \
    -out {{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.csr
  when:
    ansible_hostname in groups['cluster-master'] and
    (ssl_api_server_regenerate|bool == true or kube_controller_manager_key_stat.stat.exists == false 
    or kube_controller_manager_csr_stat.stat.exists == false)

- name: Stat kube controller manager certificate file
  delegate_to: localhost
  stat:
    path: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.crt"
  register: kube_controller_manager_cert_stat

- name: Sign kube controller manager certificate with root CA
  delegate_to: localhost
  shell: |
    openssl x509 -req \
    -extfile {{ ssl_api_server_dir }}/{{ ansible_hostname }}/node.conf \
    -extensions client \
    -CA {{ ssl_api_server_dir }}/rootCA.crt \
    -CAkey {{ ssl_api_server_dir }}/rootCA.key \
    -CAcreateserial \
    -days {{ ssl_api_server_days_valid }} \
    -in {{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.csr \
    -out {{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.crt
  when:
    ansible_hostname in groups['cluster-master'] and
    (ssl_api_server_regenerate|bool == true or kube_controller_manager_cert_stat.stat.exists == false)     

# Папки на сервере
- name: Creates kube controller manager directory
  file: 
    path: "{{ item }}"
    state: directory
  with_items:
    - "{{ kube_controller_manager_dir }}"
    - /var/lib/kube_controller_manager
  when: 
    ansible_hostname in groups['cluster-master']

# Копирование сертификатов
- name: Copy api server root ca file
  copy:
    src: "{{ ssl_api_server_dir }}/rootCA.crt"
    dest: "/etc/ssl/certs/api_server/rootCA.crt"
  register:
    api_server_ca_changed

- debug:
    msg: Restart kube controller manager when root CA changed
  when: 
    ansible_hostname in groups['cluster-master'] and api_server_ca_changed.changed == true
  notify:
    - kube_controller_manager restart

- name: Copy kube controller manager key file
  copy:
    src: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.key"
    dest: "/etc/ssl/private/api_server/kube_controller_manager.key"
    mode: 0600
  when:
    ansible_hostname in groups['cluster-master']    
  notify:
   - kube_controller_manager restart

- name: Copy kube controller manager cert file
  copy:
    src: "{{ ssl_api_server_dir }}/{{ ansible_hostname }}/kube_controller_manager.crt"
    dest: "/etc/ssl/certs/api_server/kube_controller_manager.crt"
  when:
    ansible_hostname in groups['cluster-master']    
  notify:
   - kube_controller_manager restart

- name: Copy service account key file
  copy:
    src: "{{ ssl_api_server_dir }}/service_account.key"
    dest: "/etc/ssl/private/api_server/service_account.key"
    
# Создание сервиса
- name: Generate kube controller manager config
  template:
    src: kube_controller_manager.j2
    dest: "/var/lib/kube_controller_manager/kubeconfig"
  when:
    ansible_hostname in groups['cluster-master']       
  notify:
    - kube_controller_manager restart

- name: Create kube controller manager docker-compose file
  template:
    src: 'docker-compose.yml.j2'
    dest: "{{ kube_controller_manager_dir }}/docker-compose.yml"
  when: 
    ansible_hostname in groups['cluster-master']
  notify:
    - kube_controller_manager restart

- name: Create kube controller manager systemd service service
  template:
    src: "kube_controller_manager.service.j2"
    dest: "/etc/systemd/system/kube_controller_manager.service"
  when: 
    ansible_hostname in groups['cluster-master']
  notify:
    - kube_controller_manager restart

- name: Enable and start kube controller manager
  systemd:
    name: kube_controller_manager
    state: started
    daemon_reload: yes
    enabled: true
  when: 
    ansible_hostname in groups['cluster-master']

- name: Flush handlers
  meta: flush_handlers     